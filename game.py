import random
import PySimpleGUI as sg

#random number
n = random.randint(1, 99)

#create layout
titleText = sg.Text("Enter a number from 1 to 99")
userInput = sg.Input("", enable_events=True, key="-INPUT-")
submitButton = sg.Button("Guess", bind_return_key=True)
responseText = sg.Text("", key="-OUTPUT-")

layout = [[titleText], [userInput], [submitButton], [responseText]]

#create the window
window = sg.Window("Number guesser", layout, size=(750,150))

#create an event loop
while True:
    event, values = window.read()
    #guess events
    if event == "Guess":
        if int(values["-INPUT-"]) < n:
            window["-OUTPUT-"].update("Too low!")
        elif int(values["-INPUT-"]) > n:
            window["-OUTPUT-"].update("Too high!")
        else:
            sg.popup("You guessed it right!")

    #close window
    if event == sg.WIN_CLOSED:
        break

window.close()
