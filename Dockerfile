FROM python:3
WORKDIR /usr/src/app
COPY game.py ./
CMD [ "python", "./game.py"]